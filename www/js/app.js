// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var tcpServer = null;
var socketId = null;
var server_ip = '192.168.43.1';

// cordova plugin add https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-sockets-tcpServer.git
// cordova plugin add cordova-plugin-networkinterface

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

var dumpedString_data = '';
var dumped_string_buff = str2ab('');

angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.factory('pouch', function(){
  var localDB;

  var service = {
    initialize: initialize
  }

  return service;

  function initialize(db_name){
    localDB = new PouchDB(db_name, {
      adapter: 'websql',
      auto_compaction: true
    });
    return localDB;
  }
})
.controller('mainCtrl', function($scope, $http, $cordovaFile, $ionicPlatform, pouch){
  console.log('mainCtrl');
  $scope.syncserver_ip = '192.168.1.12:3000';
  $scope.couchdb_server = 'jspblm:kingcouch13@couchdb.jspblm.com:5984';
  $scope.db_name = 'user_sync1';
  $scope.dumped_string = false;
  $scope.status_log = 'status';
  var pouchdb = pouch.initialize($scope.db_name);
  pouchdb.setMaxListeners(30);
  // var dumpedString_data = '{"version":"1.2.4","db_type":"websql","start_time":"2016-01-11T23:15:18.584Z","db_info":{"doc_count":29,"update_seq":29,"sqlite_plugin":false,"websql_encoding":"UTF-16","db_name":"user_sync1","auto_compaction":true,"adapter":"websql"}}{"docs":[{"name":"ua","profession":"pa","_id":"9fbdb213-f9b5-4d10-9d1c-2c7deea0b303","_rev":"1-d139d00a05a63be6625e91dc19aeb84f"}]}{"seq":1}{"docs":[{"_id":"c2890e22-0096-4692-a3bf-2c5daaf52bb0","_rev":"1-ac9a4ccd9035ac11a8af97db0c4966f7"}]}{"seq":2}{"docs":[{"name":"cel1","profession":"cep1","_id":"1a00c07c-de3d-4cb8-8bf3-3b8d45036031","_rev":"1-80dd94aefa085e4988b431d71f22ad8b"},{"name":"vhh","profession":"ggj","_id":"2f6eb093-f58e-4978-ac39-d2c0d91b039a","_rev":"1-73ce829b008764dfaf6120d83713e14e"},{"name":"xz","profession":"xz","_id":"3dffe153-9030-487f-802c-64fc3f0599d2","_rev":"1-d38d029245efca1b3be185b207de7949"},{"name":"user 1","profession":"pro 1","_id":"5da9a6cc-66c2-48c2-8f07-b50bc85b5c07","_rev":"1-34c4c09bf1725d6cd6e2a046743e2d1b"},{"name":"nb","profession":"pb","_id":"6003a207-c80d-424a-981e-08e79bf21b74","_rev":"1-1b35c6c2afeb10f633144c9f03fa3c78"},{"name":"1","profession":"1","_id":"611f368a-d5e8-475f-8ee0-fb8a22b45c07","_rev":"2-6308c463015aeefdd4fd3934ebffc1fc","_revisions":{"start":2,"ids":["6308c463015aeefdd4fd3934ebffc1fc","dfc7e87d335cbda85a9627e0e7d1539a"]}},{"name":"nl","profession":"pl","_id":"6650c346-8b01-4c72-b28c-080fbb0b768f","_rev":"1-450ea6e09bd631431e71ac4dfadc12ec"},{"name":"na","profession":"pa","_id":"710a461f-955a-440d-8915-4aae6d193800","_rev":"1-75e5ba8ce566f9f1cea5f0d334f986b7"},{"name":"user 2","profession":"pro 2","_id":"7496ac72-733e-42d9-bb47-3a8999d74d83","_rev":"1-f5dfb162c713e749495b6b8c30cf3368"},{"name":"ne","profession":"pe","_id":"7b220c14-3772-4d69-a60d-4aa891585dc9","_rev":"1-7b5d5083d839e2ca3066b765ced0c725"},{"name":"p3","profession":"hp3","_id":"7b231637-ee67-495c-a0ad-8828cc53c334","_rev":"1-dc4cf66dfe2db3c974fcd0f01024b28c"},{"name":"nf","profession":"pf","_id":"7efff5df-cb94-4e9a-81c7-2a714c5f5c66","_rev":"1-a8a2a91667ef630f57322d7a7edac9ea"},{"name":"h1","profession":"hp1","_id":"82959bc4-6b9c-496a-bd24-fbfc78fe747b","_rev":"1-939308472b2440c330cc14cde3d6e017"},{"name":"nd","profession":"pd","_id":"84e2af3d-2b12-4b0c-84eb-add2df5b1400","_rev":"1-67df32381ec2614bd8b450618f7c7236"},{"name":"h2","profession":"p2","_id":"8571277b-f09e-4746-8cee-989c2625d71f","_rev":"1-25ae015ef8239a7cf5ff3fc43a7e8c82"},{"name":"ser 1 - edit","profession":"ser 1 - edit","_id":"a58d6bc4d7b5ff3dcebaf0715e04265c","_rev":"2-e1063ffe21a1619066a7ab64cfc12a16","_revisions":{"start":2,"ids":["e1063ffe21a1619066a7ab64cfc12a16","5af8ed501ebfd657095e0be0b61528a7"]}},{"name":"ser 2 - edit","profession":"ser 2 - edit","_id":"a58d6bc4d7b5ff3dcebaf0715e042923","_rev":"2-504eee673cba5a43d6c4b605019c1ee3","_revisions":{"start":2,"ids":["504eee673cba5a43d6c4b605019c1ee3","86c4c0f448acf006d7aa83b5bc0ded74"]}},{"name":"nuc 1","profession":"nuc 1","_id":"a58d6bc4d7b5ff3dcebaf0715e0436b2","_rev":"1-806f5315c8dc346b6542dd63a00451ad"},{"name":"abc","profession":"abc","_id":"b156b581-73ab-42f1-a878-7b1e595f5704","_rev":"1-5d7569eef2069af850ccb1e6b148884c"},{"name":"cel 2","profession":"cep 2 gh","_id":"b1725d16-23b4-45c3-ad54-9dd0ae3886b7","_rev":"1-43521a661a3538a95283518ed5031d45"},{"name":"nc","profession":"pc","_id":"b9baa870-0505-43ce-b094-43296ecde8c6","_rev":"1-a5ba1bb5874aba3e3bb03190feac04e4"},{"name":"ng","profession":"pg","_id":"d42177d0-e01d-4f77-ba6d-04adfd2d0a26","_rev":"1-e70aa7815052414f452b384e79a645c2"},{"name":"nh","profession":"ph","_id":"da28e1b9-43fb-4b06-a9c0-5aab2733fa28","_rev":"1-4a031d14b90c81982de1088c81f9bb50"},{"name":"hh","profession":"ffh","_id":"e3113a00-3d07-4b39-8400-e26657819290","_rev":"1-26f256cf427cbdedb1b07318c0752bc4"},{"name":"ni","profession":"pi","_id":"e3d20103-d6d1-4bef-860d-68fa98e50d1a","_rev":"1-0a86cf71e0cfddce30322b6b7b9dc942"},{"name":"fhg","profession":"syt","_id":"ef78fdfc-3ecf-4b57-9948-db44c3774fbb","_rev":"1-408c1e1948a80ba0f6ea98831277e077"},{"name":"nj","profession":"pj","_id":"f8a725ff-e5fc-4d0a-a0a1-9564e12d19de","_rev":"1-3893bbf5f41c84c7bcd830aec8eb6acb"}]}{"seq":29}';
  // var dumpedString_data = '';
  // var dumped_string_buff = '';
  // =========== TCP Server >>> =========
  $ionicPlatform.ready(onReady);

  function onReady(){
    networkinterface.getIPAddress(function(server_ip){
      console.log('server_ip', server_ip);
      $scope.$apply(function(){
        $scope.server_ip = server_ip;
      });
      chrome.sockets.tcpServer.create({}, function(createInfo) {
        listenAndAccept(createInfo.socketId, server_ip);
      });
    }, function(info){
      console.log('Error getIPAddress', info);
    });
  }

  function listenAndAccept(socketId, server_ip) {
    console.log('listenAndAccept', socketId, server_ip);
    console.log('typeof ', typeof server_ip);
    chrome.sockets.tcpServer.listen(socketId,
      server_ip, 8000, function(resultCode) {
        $scope.$apply(function(){
          $scope.status_log = 'listen' + resultCode;
        });
        onListenCallback(socketId, resultCode);
    }, function(error){
      console.log('Error listen', error);
    });
  }

  var serverSocketId;
  function onListenCallback(socketId, resultCode) {
    console.log('onListenCallback', socketId, resultCode);
    if (resultCode < 0) {
      console.log("Error listening:" +
      chrome.runtime.lastError.message);
      return;
    }
    serverSocketId = socketId;
    chrome.sockets.tcpServer.onAccept.addListener(onAccept);
  }

  function onAccept(info) {
    console.log('onAccept', info);
    if (info.socketId != serverSocketId)
      return;

    // A new TCP connection has been established.
    var data = str2ab("Data sent to new TCP client connection.");
    chrome.sockets.tcp.send(info.clientSocketId, data,
      function(resultCode) {
        console.log("Data sent to new TCP client connection.", resultCode);
    });

    // Start receiving data.
    chrome.sockets.tcp.onReceive.addListener(function(recvInfo) {
      console.log('onReceive');
      if (recvInfo.socketId != info.clientSocketId){
        console.log('return recvInfo.socketId != info.clientSocketId');
        return;
      }
      // dumped_string_buff = Buffer.concat([dumped_string_buff, recvInfo.data]);
      dumpedString_data += ab2str(recvInfo.data);

        // recvInfo.data is an arrayBuffer.
      // var rec_data = ab2str(recvInfo.data);
      // dumpedString_data += rec_data;
      // console.log('onReceiveStr', rec_data);
      // $scope.dumped_string = true;
      // $scope.$apply(function(){
      //   $scope.rec_data = rec_data;
      // });


    });

    chrome.sockets.tcp.onReceiveError.addListener(function(onReceiveErrorinfo) {
      console.log("onReceiveError: ", onReceiveErrorinfo);
    });
    chrome.sockets.tcp.setPaused(info.clientSocketId, false);
  }
  // =========== <<< TCP Server =========

  $scope.print_dumpedString_data = function(){
    // dumpedString_data = ab2str(dumped_string_buff);
    console.log('dumpedString_data', dumpedString_data);
  }

  $scope.empty_dumpedString_data = function(){
    dumpedString_data = '';
    console.log('empty dumpedString_data', dumpedString_data);
  }

  $scope.upload_to_server = function(){
    console.log('upload_to_server');
    $http
      .post(
        'http://' + $scope.syncserver_ip + '/pouchdb_dump_file/',
        dumpedString_data
        ,{
          headers : {
            'Content-Type' :
            'application/x-www-form-urlencoded; charset=UTF-8'
          }
        }
      )
      .then(
        function(response){
          console.log("Upload_to_server Response !!!", response);
          $scope.upload_to_server_status = 'uploaded! ' + response;
        },
        function(error){
          console.log("Upload_to_server Error !!!", error);
          $scope.upload_to_server_status = 'Error uploaded! ' + error;
      });
  }

  $scope.load_dump_to_couchdb_server = function(){
    console.log("sync_dump_to_server");
    db_name = 'user_sync1';
    var db = new PouchDB('http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + db_name);
    console.log("db.load === ", db.load);
    db.load(dumpedString_data, {
        proxy: 'http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + db_name
      })
    	.then(function(res){
    			console.log("Response", res);
          dumpedString_data = '';
    		})
    	.catch(function(err){
    			console.debug(err);
    		});
  }

  $scope.load_dump_to_local_pouchdb = function(){
    pouchdb.load(dumpedString_data)
      .then(function(){
        console.log('Done local loading!');
        dumpedString_data = '';
      })
      .catch(function(err){
        console.log('Error local loading!!!');
      })
  }

  $scope.sync_local_to_couchdb = function(){
    var sync_process = pouchdb.sync('http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + $scope.db_name,
        {live:true,
        retry:true})
        .on('change', function(info){
          console.log("change", info);
        })
        .on('paused', function(){
          console.log('paused, now cancel!');
          sync_process.cancel();
        })
        .on('active', function(){
          console.log('active');
        })
        .on('denied', function(info){
          console.log('denied', info);
        })
        .on('complete', function(info){
          console.log('complete', info);
        })
        .on('error', function(err){
          console.log('error', err);
        });
  }

  $scope.write_dump_to_file = function(){
    $cordovaFile.writeFile(cordova.file.externalDataDirectory,
      $scope.db_name + "_dump.txt", dumpedString_data, true)
      .then(function(success){
        console.log('writeFile success', success);
        $scope.write_file_status = 'writed!';
      }, function(error){
        console.log('writeFile error', error);
      });
  }

  $scope.readFile = function(){
    console.log('readFile');
    $cordovaFile.readAsText(
      cordova.file.externalDataDirectory,
      $scope.db_name + '_dump.txt')
      .then(function(response){
        console.log('readAsTextSuccess -> ', response);
        dumpedString_data = response;
        $scope.dumped_string = true;
        $scope.read_file_status = 'read! ';
      }, function(response){
        console.log('readAsTextError -> ', response);
        $scope.read_file_status = 'Error read! ' + response;
      });
  }

  $scope.deleteFile = function(){
    console.log('deleteFile');
    $cordovaFile.removeFile(
      cordova.file.externalDataDirectory,
      'user_sync1_dump.txt'
    ).then(function(response){
      console.log('removeFileSuccess', response);
      $scope.delete_file_status = 'success!' + response;
    }, function(response){
      console.log('removeFileError', response);
      $scope.delete_file_status = 'error!' + response;
    });
  }

  $scope.see_all_docs = function(){
      pouchdb.allDocs({
        include_docs: true
      }).then(function(result){
        console.log("result.rows === ", result.rows);
        $scope.docs = result.rows;
      }).catch(function(err){
        console.log("Error allDocs === ", err);
      })
    }

})
